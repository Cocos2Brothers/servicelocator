#include <iostream>


class IAudioSystem
{
public:
    virtual void playSound() = 0;
};


class AudioSystem : public IAudioSystem
{
public:
    virtual void playSound()
    {
        std::cout << " Audio System :: playSound() " << std::endl;
    }
};


class LoggedAudio : public IAudioSystem
{
    IAudioSystem * wrapped_;
public: 
    LoggedAudio( IAudioSystem * wr ) : wrapped_( wr ) {};
    virtual void playSound()
    {
        std::cout << "\n Logs Enabled " << std::endl;
        wrapped_->playSound();
    }
};

class NullAudio : public IAudioSystem
{
public: 
    virtual void playSound()
    {
        std::cout << " empty behavior \n" << std::endl;
    }
};


class ServiceLocator
{
public:
    static IAudioSystem * getAudioSystem()
    {
        if ( audioSystem_ == nullptr )
        {
            audioSystem_ = new NullAudio;
        }
        return audioSystem_;
    }

    static void setAudioSystem( IAudioSystem * as )
    {
        audioSystem_ = as;
    }

private:
    static IAudioSystem * audioSystem_;
};
IAudioSystem * ServiceLocator::audioSystem_ = nullptr;


int main()
{

    IAudioSystem * testAudioSystem  = ServiceLocator::getAudioSystem();
    testAudioSystem->playSound();
    

    IAudioSystem * audioSytem = new AudioSystem;
    ServiceLocator::setAudioSystem( audioSytem );
    testAudioSystem = ServiceLocator::getAudioSystem(); 
    testAudioSystem->playSound();

    IAudioSystem * testWrappedAudio = new LoggedAudio( audioSytem );
    ServiceLocator::setAudioSystem( testWrappedAudio );
    testAudioSystem = ServiceLocator::getAudioSystem();
    testAudioSystem->playSound();
    

    system( "pause" );
    return 0; 
}